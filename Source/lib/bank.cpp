#include "bank.h"

#include <algorithm>
#include <cassert>
#include <limits>
#include <sstream>

using namespace AccountVariant;

Id GenerateUniqueId()
{
    static int counter = 0;
    std::stringstream stream;
    stream << counter;
    counter += 1;
    return Id{stream.str()};
}

AccountBase::AccountBase(Id id)
    : id_(std::move(id))
    , value_(0)
{
}

Id AccountBase::GetID() const { return id_; }

double AccountBase::GetValue() const { return value_; }

void AccountBase::SetValue(double d) { value_ = d; }

InstantAccessAccount::InstantAccessAccount(const Id& id)
    : AccountBase(id)
{
}

NoInterestAccount::NoInterestAccount(const Id& id)
    : AccountBase(id)
{
}

bool AccountVariant::operator==(const Id& lhs, const Id& rhs)
{
    return lhs.value == rhs.value;
}

bool AccountVariant::operator!=(const Id& lhs, const Id& rhs)
{
    return !(lhs == rhs);
}

template <typename T>
Id Bank::OpenAccount()
{
    auto id = GenerateUniqueId();
    accounts_.emplace_back(Account(T(id)));
    return id;
}

// explicit instances of available types - will cause compile errors on other
// types
template AccountVariant::Id Bank::OpenAccount<InstantAccessAccount>();
template AccountVariant::Id Bank::OpenAccount<NoInterestAccount>();

size_t Bank::GetNumberOfOpenAccounts() const { return accounts_.size(); }

double DepositToAccount(Account* account, double amount)
{
    if (account) {
        std::visit([&](auto& a) { a.SetValue(amount + a.GetValue()); },
                   *account);
        return std::visit([](auto& a) { return a.GetValue(); }, *account);
    }
    return std::numeric_limits<double>::quiet_NaN();
}

double AccountVariant::Bank::Deposit(const Id& accountId, double amount)
{
    auto account = find(accountId);
    return DepositToAccount(account, amount);
}

double ReadBalance(const Account* account)
{
    if (account) {
        return std::visit([](auto& a) { return a.GetValue(); }, *account);
    }
    return std::numeric_limits<double>::quiet_NaN();
}

double AccountVariant::Bank::GetBalance(const Id& accountId) const
{
    auto account = find(accountId);
    return ReadBalance(account);
}

struct WithdrawPolicy
{
    double amount;

    template <typename T>
    double operator()(T& account)
    {
        auto balance = account.GetValue();
        auto to_withdraw = std::min(balance, amount);
        account.SetValue(balance - to_withdraw);
        return to_withdraw;
    }

    double operator()(NoInterestAccount& account)
    {
        auto balance = account.GetValue();
        if (balance >= amount) {
            account.SetValue(balance - amount);
            return amount;
        }
        return 0;
    }
};

double WithdrawFromAccount(Account* account, double amount)
{
    if (account) {
        WithdrawPolicy w{amount};
        return std::visit(w, *account);
    }
    return 0.0;
}

double AccountVariant::Bank::Withdraw(const Id& accountId, double amount)
{
    auto account = find(accountId);
    return WithdrawFromAccount(account, amount);
}

struct InterestPolicy
{
    void operator()(InstantAccessAccount& account)
    {
        const auto balance = account.GetValue();
        auto new_balance = balance * (1 + InstantAccessAccount::InterestRate);
        account.SetValue(new_balance);
    }

    template <typename T>
    void operator()(const T&) {}
};

void PayInterestToAccount(Account& account)
{
    InterestPolicy p;
    std::visit(p, account);
}

void AccountVariant::Bank::PayInterest()
{
    std::for_each(accounts_.begin(), accounts_.end(), &PayInterestToAccount);
}

const Account* AccountVariant::Bank::find(const Id& id) const
{
    const Account* result = nullptr;
    auto iter = std::find_if(
        accounts_.begin(), accounts_.end(), [&](const Account& account) {
            return std::visit([&](auto& a) { return a.GetID() == id; },
                              account);
        });

    if (iter != accounts_.end()) {
        result = &(*iter);
    }

    return result;
}

// implement non-const version based on const version
Account* AccountVariant::Bank::find(const Id& id)
{
    return const_cast<Account*>(static_cast<const Bank*>(this)->find(id));
}
