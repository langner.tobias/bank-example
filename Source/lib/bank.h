#include <optional>
#include <string>
#include <variant>
#include <vector>

namespace AccountVariant
{
    struct Id
    {
        std::string value;
    };

    bool operator==(const Id& lhs, const Id& rhs);
    bool operator!=(const Id& lhs, const Id& rhs);

    class AccountBase
    {
    public:
        AccountBase(Id id);

        // movable but not copyable. Applies to all subtypes
        AccountBase(const AccountBase&) = delete;
        AccountBase& operator=(const AccountBase&) = delete;
        AccountBase(AccountBase&&) = default;
        AccountBase& operator=(AccountBase&&) = default;

        Id GetID() const;
        double GetValue() const;

        void SetValue(double d);

    private:
        Id id_;
        double value_;
    };

    class InstantAccessAccount : private AccountBase
    {
    public:
        constexpr static double InterestRate = 0.001;

        InstantAccessAccount(const Id& id);

        using AccountBase::GetID;
        using AccountBase::GetValue;
        using AccountBase::SetValue;
    };

    class NoInterestAccount : private AccountBase
    {
    public:
        NoInterestAccount(const Id& id);

        using AccountBase::GetID;
        using AccountBase::GetValue;
        using AccountBase::SetValue;
    };

    using Account = std::variant<InstantAccessAccount, NoInterestAccount>;

    class Bank
    {
    public:
		Bank() = default;
        //movable but not copyable
        Bank(const Bank&) = delete;
        Bank& operator=(const Bank&) = delete;
        Bank(Bank&&) = default;
        Bank& operator=(Bank&&) = default;

        template <typename T>
        Id OpenAccount();

        size_t GetNumberOfOpenAccounts() const;
        double Deposit(const Id& accountId, double amount);
        double GetBalance(const Id& accountId) const;
        double Withdraw(const Id& accountId, double amount);

        void PayInterest();

    private:
        const Account* find(const Id& id) const;
        Account* find(const Id& id);

        std::vector<Account> accounts_;
    };
}