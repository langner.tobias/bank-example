# Bank example based on variant instead of inheritance

Initial reasoning taken from: http://sean-parent.stlab.cc/presentations/2017-01-18-runtime-polymorphism/2017-01-18-runtime-polymorphism.pdf

> Inheritance is the base class of Evil

## Problems with inheritance in C++

- By using inheritance to capture polymorphic use, we shift the burden of use to the type
implementation, tightly coupling components
- Inheritance implies variable size, which implies heap allocation
- Heap allocation forces a further burden to manage the object lifetime
- Indirection, heap allocation, and virtualization, impacts performance
- Object lifetime management leads to garbage collection or reference counting
- This encourages shared ownership and the proliferation of incidental data-structures
- Shared ownership leads to synchronization issues, breaks local reasoning, and further impacts
performance

> The requirement for polymorphism comes from its use, not from not from the type.
> There is no polymorphic type, only polymorphic use of similar types.

# Design

## class Bank

class Bank uses a vector of variant<AccountTypes ...> to manage the accounts. All 
functionality is realized in form of the std::visit when traversing this vector.

The variant may contain any type that satisfies the interface of an account.
A bank must not be copyable. It would otherwise duplicate the accounts.

Accounts are created by a templated create method. This method is explicitly
instanciated for each account type.

### Possible extensions

- make Bank copyable by making the vector of accounts shared across all instances 
(e.g. with a shared_ptr). This should also require making it thread-safe.
- make Bank thread-safe

## Account

An account can be used by a bank to provide certain customizable functionality.
To provide functionality that is different from the default, a new type is needed.
The type must provide at least:
- Constructor from Id
- GetId()
- GetValue()
- SetValue()

### Customizing accounts

To customize the behavior of an account type, the appropriate policy needs to be
adapted by providing an overload for the account type. The default behavior
is provided by a templated overload.

### Possible extensions
- provide "deposit" policy (e.g. with fees)
- provide different id mechanism (e.g. "Nummernkonto")
- provide different base number type either globally or locally

# Todo
- separate extension code from non-extension code

