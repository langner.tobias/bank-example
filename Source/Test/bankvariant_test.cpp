#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "bank.h"
#include <vector>

using namespace AccountVariant;

template <typename T>
void test_initialization_id()
{
    Id id{"id"};
    T account(id);
    CHECK(id == account.GetID());
}

TEST_CASE("initialization of InstantAccessAccount - id", "[AccountVariant]")
{
    test_initialization_id<InstantAccessAccount>();
}

TEST_CASE("initialization of NoInterestAccount - id", "[AccountVariant]")
{
    test_initialization_id<NoInterestAccount>();
}

template <typename T>
void test_initialization_value()
{
    Id id{"id"};
    T account(id);
    CHECK(account.GetValue() == 0.0);
}

TEST_CASE("initialization of InstantAccessAccount - value", "[AccountVariant]")
{
    test_initialization_value<InstantAccessAccount>();
}

TEST_CASE("initialization of NoInterestAccount - value", "[AccountVariant]")
{
    test_initialization_value<NoInterestAccount>();
}

TEST_CASE("initial account number == 0", "[AccountVariant]")
{
    Bank b;
    CHECK(b.GetNumberOfOpenAccounts() == 0);
}

TEST_CASE("opening accounts increases number of open accounts",
          "[AccountVariant]")
{
    Bank b;
    b.OpenAccount<InstantAccessAccount>();
    CHECK(b.GetNumberOfOpenAccounts() == 1);
    b.OpenAccount<NoInterestAccount>();
    CHECK(b.GetNumberOfOpenAccounts() == 2);
}

TEST_CASE("created IDs are unique", "[AccountVariant]")
{
    Bank b;
    const int count = 100;
    std::vector<Id> ids;
    ids.reserve(count);
    for (int i = 0; i < count; ++i)
    {
        auto id = b.OpenAccount<InstantAccessAccount>();
        auto result = std::find(ids.begin(), ids.end(), id);
        CHECK(result == ids.end());
        ids.push_back(id);
    }
}

template <typename T>
void initial_balance_0()
{
    Bank b;
    auto id = b.OpenAccount<T>();
    CHECK(b.GetBalance(id) == 0);
}

TEST_CASE("initial balance of account is empty - InstantAccessAccount",
          "[AccountVariant]")
{
    initial_balance_0<InstantAccessAccount>();
}

TEST_CASE("initial balance of account is empty - NoInterestAccount",
          "[AccountVariant]")
{
    initial_balance_0<NoInterestAccount>();
}

template <typename T>
void deposit_increases()
{
    Bank b;
    auto id = b.OpenAccount<T>();
    const double amount = 100.0;
    CHECK(b.Deposit(id, amount) == amount);
    CHECK(b.Deposit(id, amount) == 2 * amount);
    CHECK(b.GetBalance(id) == 2 * amount);
}

TEST_CASE("deposit increases balance - InstantAccessAccount",
          "[AccountVariant]")
{
    deposit_increases<InstantAccessAccount>();
}

TEST_CASE("deposit increases balance - NoInterestAccount", "[AccountVariant]")
{
    deposit_increases<NoInterestAccount>();
}

template <typename T>
void withdraw_reduces_balance()
{
    Bank b;
    auto id = b.OpenAccount<T>();
    const double amount = 100.0;
    b.Deposit(id, amount);
    CHECK(b.Withdraw(id, amount / 2) == amount / 2);
    CHECK(b.GetBalance(id) == amount / 2);
}

TEST_CASE("withdraw reduces balance - InstantAccessAccount", "[AccountVariant]")
{
    withdraw_reduces_balance<InstantAccessAccount>();
}

TEST_CASE("withdraw reduces balance - NoInterestAccount", "[AccountVariant]")
{
    withdraw_reduces_balance<NoInterestAccount>();
}

TEST_CASE(
    "withdraw does not withdraw more than the balance - InstantAccessAccount",
    "[AccountVariant]")
{
    Bank b;
    auto id = b.OpenAccount<InstantAccessAccount>();
    const double amount = 100.0;
    b.Deposit(id, amount);
    CHECK(b.Withdraw(id, amount + 1) == amount);
    CHECK(b.GetBalance(id) == 0);
}

TEST_CASE("withdraw does not withdraw if amount > than the balance - "
          "NoInterestAccount",
          "[AccountVariant]")
{
    Bank b;
    auto id = b.OpenAccount<NoInterestAccount>();
    const double amount = 100.0;
    b.Deposit(id, amount);
    CHECK(b.Withdraw(id, amount + 1) == 0.0);
    CHECK(b.GetBalance(id) == amount);
}

TEST_CASE("NoInterestAccount does not pay interest", "[AccountVariant]")
{
    Bank b;
    auto id = b.OpenAccount<NoInterestAccount>();
    const double amount = 100.0;
    b.Deposit(id, amount);
    b.PayInterest();
    CHECK(b.GetBalance(id) == amount);
}

TEST_CASE("InstantAccessAccount pays interest", "[AccountVariant]")
{
    Bank b;
    auto id = b.OpenAccount<InstantAccessAccount>();
    const double amount = 100.0;
    b.Deposit(id, amount);
    b.PayInterest();
    CHECK(b.GetBalance(id) ==
          amount * (1 + InstantAccessAccount::InterestRate));
}

TEST_CASE("retrieval of balance with wrong id yields quiet nan",
          "[AccountVariant]")
{
    Bank b;
    b.OpenAccount<InstantAccessAccount>();
    b.OpenAccount<NoInterestAccount>();
    Id illegalId{"illegal"};
    // NaN is the only value that is not equal to itself
    CHECK(b.GetBalance(illegalId) != b.GetBalance(illegalId));
}

TEST_CASE("withdraw with wrong id 0", "[AccountVariant]")
{
    Bank b;
    auto account1 = b.OpenAccount<InstantAccessAccount>();
    auto account2 = b.OpenAccount<NoInterestAccount>();
    b.Deposit(account2, 200);
    b.Deposit(account1, 100);
    Id illegalId{"illegal"};
    CHECK(b.Withdraw(illegalId, 10) == 0);
    CHECK(b.GetBalance(account1) == 100);
    CHECK(b.GetBalance(account2) == 200);
}
